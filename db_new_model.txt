create table category(
cat_num varchar(256) primary key,
cat_name varchar(256) not null
);
create table product(
prod_id integer primary key,
prod_name varchar(256),
price integer not null,
installment_plan boolean not null,
warranty_period integer not null, --� �������
img_url varchar(256) not null,
description varchar(256) not null,
cat_num varchar(256) not null
);
INSERT INTO category(cat_num, cat_name) VALUES
('1', 'computers'),
('1.1', 'system_block'),
('1.2', 'laptop'),
('1.2.1', 'ultrabook'),
('2', 'accessories'),
('2.1', 'fan');
INSERT INTO product(prod_id, prod_name, price, installment_plan, warranty_period, img_url, description, cat_num) VALUES
(1, 'sblock_1', 10000, true, 3, 'http://crimeastokcomputer.ru/wp-content/uploads/2019/02/IMG_3286.jpg', 'sblock_1', '1.1'),
(2, 'sblock_2', 20000, false, 4, 'https://��������.���/image/cache/catalog/desktops/Gamemax/igrovoi-pk-magnum-550-17-1200x800.jpg', 'sblock_2', '1.1'),
(3, 'sblock_3', 30000, true, 5, 'http://crimeastokcomputer.ru/wp-content/uploads/2019/02/IMG_3286.jpg', 'not sblock_1', '1.1'),
(4, 'UBOOK_1', 40000, false, 4, 'https://static.21vek.by/img/galleries/1026/209/preview_b/x543magq555_asus_5d14d9d93d40b.jpeg', 'not sblock_1', '1.2.1'),
(5, 'UBOOK_1', 45000, true, 4, 'https://snr.ru/cdn/article/498/hp_elitebook_x360_1030_g2_1em86ea_1.jpg', 'not sblock_1', '1.2.1'),


--��������� 4 ������� ��� ������� � cat_num = 1.1 (��� ��������� � ��������
���������� ���������� ������ �����������)
1)������� ���� ������
select avg(price) from product where cat_num = '1.1';
--2000
2.1)5 ����� ������� ������� (��� ������ 5)
select *
from product
where cat_num = '1.1'
order by price asc
limit 5;
2.2)5 ����� ������� ������� (��� ������ 5)
select *
from product
where cat_num = '1.2.1'
order by price desc
limit 5;
3)������, �� ������� ���� ���������
select * from product
where installment_plan = true and cat_num = '1.2.1';
4)������ � ����������� ������ ������, ��� N �������
select * from product
where warranty_period > 9 and cat_num = '2.1';
(6, 'fan_1', 1000, false, 10, 'http://crimeastokcomputer.ru/wp-content/uploads/2019/02/IMG_3286.jpg', 'not sblock_1', '2.1'),
(7, 'fan_2', 1500, true, 9, 'http://crimeastokcomputer.ru/wp-content/uploads/2019/02/IMG_3286.jpg', 'not sblock_1', '2.1');
